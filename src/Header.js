import React, { Component }  from 'react';
import logo from './images/eb-logo.svg';
function Header(){

    function logout(value) {
        sessionStorage.removeItem("loginFlag");
        window.location.reload();
    }
    return (      
    <header>
        <div className="container">
           <a class="logo" href="index.html" title="Easebuzz Merchant Analysis">
               <img src={logo} alt="Easebuzz"/>
            </a>
            <a className="Log_Out_a" href="javascript:void(0)" onClick={e => logout(e.target.value)} style={{color:"#fff"}}>Log Out</a>
        </div>
       
     </header>)
}
export default Header;
