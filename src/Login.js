import React, { Component }  from 'react';
import Header from './Header';
import './css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from './images/eb-logo.svg';
import Footer from './Footer';
import { useState, useEffect  } from "react";
import axios from 'axios';
import firebase from './firebase';
import Home  from './Home';
function setLoginSessionFlag() {
    sessionStorage.setItem('loginFlag', true);
}
function Login(){
    

    const [email, setEmail] = useState("");
    const [loginFlag, setLoginFlag] = useState(false);
    const [verifyFlag, setVerifyFlag] = useState(false);
    const [password, setPassword] = useState("");
    const [token, setToken] = useState("");
    const api_key = "AIzaSyBPK4BGjxa4ByuAIqA2mb-izPFI_qEPq7U";
    const admin_username = 'ebuzz-admin';
    const admin_password = 'ph@7rePh2021';
    const loginFormSubmit = (evt) => {
       if(email.trim()==""){
            alert("Please Enter Email");
            
        }
        else if(password.trim()==""){
            alert("Please Enter Passowrd");
        }else{
            if(admin_username == email && admin_password == password ){
                setLoginSessionFlag();
                setLoginFlag(true);
            }else{
                alert('Invalid Login');
                setLoginFlag(false);
            }
            
        }
        evt.preventDefault();
    }
    function handleEmailValues(value) {
        setEmail(value);
        setLoginFlag(false);
    }
    function handlePasswordValues(value) {
        setPassword(value);
        setLoginFlag(false);
     
    }
    if(!loginFlag){
        return (<div>
        
            <section className="merchant_form_section authentication_login_section">
                <a className="logo authentication_login_lgo" href="index.html" title="Easebuzz Merchant Analysis"
                >
                    <img src={logo} alt="Easebuzz"/></a>
            <form onSubmit={loginFormSubmit}>
                <div className="row">
                    <div className="col-12">
                    <label for="password" className="col-form-label new_lab_text">UserName</label>
                    <input type="text" className="form-control" id="email" onChange={e => handleEmailValues(e.target.value)}/>
                    </div>
                    <div className="col-12">
                    <label for="passowrd" className="col-form-label new_lab_text">Password</label>
                    <input type="password" className="form-control" id="password" onChange={e => handlePasswordValues(e.target.value)}/>
                    </div>
                    <div className="col-12">
                    <button style={{display:'block'}} type="submit" className="btn btn-primary submit_button login_submit_button">Submit</button>
                   
                    </div>
                </div>
            </form>
            </section>
        <Footer></Footer>
    </div>)
      
    }else{
        return(<Home></Home>);
   }
}
export default Login;
