import { useEffect, React, useState } from "react";
import axios from "axios";
function TabDetail(props) {
  const [gstNo, setGSTNo] = useState("");
  const [age, setAge] = useState("");
  const [creationDate, setCreationDate] = useState("");
  const [blackListStatus, setBlackListStatus] = useState("");
  const [domainCategory, setDomainCategory] = useState("");
  const [riskinessStatus, setRiskinessStatus] = useState("");
  const [totalWebPage, setTotalWebPage] = useState("");
  const [termConditionPrivacy, setTermConditionPrivacy] = useState([]);
  const [numSocialMedia, setNumSocialMedia] = useState("");
  const [websiteURL, setWebsiteURL] = useState("");
  const [socialMediaLink, setSocialMediaLink] = useState([]);
  const [websiteExist, setWebsiteExist] = useState("");
  const [googleReviewCount, setGoogleReviewCount] = useState("");
  const [googleReviewRating, setGoogleReviewRating] = useState("");
  const [twitterAccountExist, setTwitterAccountExist] = useState("");
  const [twitterFollowerCount, setTwitterFollowerCount] = useState("");
  const [twitterTweetCount, setTweetCount] = useState("");
  const [domainRank, setDomainRank] = useState("");
  const [fraudData, setFraudData] = useState("");
  const [checkedMerchantID, setCheckedMerchantID] = useState("");
  const [emailID, setEmailID] = useState("");
  const [whatsappStatus, setWhatsappStatus] = useState("");
  const [businesName, setBusinessName] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [ciNumber, setCiNumber] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [dor, setDor] = useState("");
  const [cat, setCat] = useState("");
  const [classDevision, setClassDevision] = useState("");
  const [subCat, setSubCat] = useState("");
  const [roa, setRoa] = useState("");
  const [officeEmail, setOfficeEmail] = useState("");
  const [mRecordsExists, setMRecordsExists] = useState("");
  const [matchedId, setMatchedId] = useState("");
  const [fraudulentMerchant, setFraudulentMerchant] = useState("");
  const [terminatedMID, setTerminatedMID] = useState("");
  const [relMobileNumber, setRelMobileNumber] = useState("");
  const [relEmail, setRelEmail] = useState("");
  const [relWebsite, setRelWebsite] = useState("");
  const [relPanCard, setRelPanCard] = useState("");
  const [relGST, setRelGST] = useState("");
  const [countEmailChange, setCountEmailChange] = useState("");
  const [countPanChange, setCountPanChange] = useState("");
  const [countBankNameChange, setCountBankNameChange] = useState("");
  const [countImageChange, setCountImageChange] = useState("");
  const [countWebsiteChange, setCountWebsiteChange] = useState("");
  const [ezId, setEzId] = useState("");
  const [ezMName, setEzMName] = useState("");
  const [firstTransactionDate, setFirstTransactionDate] = useState("");
  const [transactionAmountMean, setTransactionAmountMean] = useState("");
  const [uniqueCustomerCount, setUniqueCustomerCount] = useState("");
  const [customerTransactionCount, setCustomerTransactionCount] = useState("");
  const [Sigma, setSigma] = useState("");
  const [recency, setRecency] = useState("");
  const [frequency, setFrequency] = useState("");
  const [monetary, setMonetary] = useState("");
  const [rfmValue, setRfmValue] = useState("");
  const [rfmAverage, setRfmAverage] = useState("");
  const [rfmCategory, setRfmCategory] = useState("");
  const [lifetimeMedianAmount, setLifetimeMedianAmount] = useState("");
  const [lifetimeRefundAmount, setLifetimeRefundAmount] = useState("");
  const [lifetimeRefundCount, setLifetimeRefundCount] = useState("");
  const [lifetimeChargebackAmount, setLifetimeChargebackAmount] = useState("");
  const [lifetimeChargebackCount, setLifetimeChargebackCount] = useState("");
  const [aggregator, setAggregator] = useState("");
  const [signupDate, setSignupDate] = useState("");
  const [loginDate, setLoginDate] = useState("");
  const [uploadDate, setUploadDate] = useState("");
  const [detailsUploadDate, setDetailsUploadDate] = useState("");
  const [infoUploadDate, setInfoUploadDate] = useState("");
  const [businessDomain, setBusinessDomain] = useState("");
  const [easeCollectMerchant, setEaseCollectMerchant] = useState("");
  const [smartBillingMerchant, setSmartBillingMerchant] = useState("");
  const [loginIPs, setLoginIPs] = useState("");
  const [eduMerchant, setEduMerchant] = useState("");
  const [bridgeBuzzMerchant, setBridgeBuzzMerchant] = useState("");
  const [wireMerchant, setWireMerchant] = useState("");
  const [loadingFlag, setLoadingFlag] = useState(false);
  const [isMerchantActive, setMerchantActive] = useState(false);
  const [isTransactionActive, setTransactionActive] = useState(false);
  const [isSocialActive, setSocialActive] = useState(true);
  const [constitutionOfBusiness, setConstitutionOfBusiness] = useState("");
  const [legalNameOfBusiness, setLegalNameOfBusiness] = useState("");
  const [centreJurisdiction, setCentreJurisdiction] = useState("");
  const [stateJurisdiction, setStateJurisdiction] = useState("");
  const [registrationDate, setRegistrationDate] = useState("");
  const [taxPayerType, setTaxPayerType] = useState("");
  const [principalPlaceAddress, setPrincipalPlaceAddress] = useState("");
  const [gstinStatus, setGstinStatus] = useState("");
  const [natureOfBusinessActivities, setNatureOfBusinessActivities] = useState(
    []
  );
  const [emailAddressId, setEmailAddressId] = useState("");
  const [validEmail, setValidEmail] = useState("");
  const [emailStatus, setEmailStatus] = useState("");
  const [freeEmail, setFreeEmail] = useState("");
  const [domainEmail, setDomainEmail] = useState("");
  const [domainAgeDays, setDomainAgeDays] = useState("");
  const [smtpProvider, setSmtpProvider] = useState("");
  const [panType, setPanType] = useState("");
  const [directorDetail, setDirectorDetail] = useState([]);
  const [supremeCourtCases, setSupremeCourtCases] = useState("");
  const [highCourtCases, setHighCourtCases] = useState("");
  const [matchedMerchantID, setMatchedMerchantID] = useState("");
  const [matchedWebsiteUrl, setMatchedWebsiteUrl] = useState([]);
  const [matchedMobileNumber, setMatchedMobileNumber] = useState([]);
  const [matchedEmail, setMatchedEmail] = useState([]);
  const [matchedPan, setMatchedPan] = useState([]);
  const [matchedGST, setMatchedGST] = useState([]);
  const [primaryMobileNumber, setPrimaryMobileNumber] = useState("");
  const [merchantState, setMerchantState] = useState("");
  const [merchantBankName, setMerchantBankName] = useState("");
  const [bankFraudCount, setBankFraudCount] = useState("");
  const [stateFraudCount, setStateFraudCount] = useState("");
  const [countMatchMerchantFraud, setCountMatchMerchantFraud] = useState("");
  const [ipRegion, setIpRegion] = useState("");
  const [merchantEmailCount, setMerchantEmailCount] = useState("");
  const [merchantPanNoCount, setMerchantPanNoCount] = useState("");
  const [merchantBankNameCount, setMerchantBankNameCount] = useState("");
  const [merchantImageCount, setMerchantImageCount] = useState("");
  const [merchantWebsiteUrlCount, setMerchantWebsiteUrlCount] = useState("");
  const showTabSection = (evt, value) => {
    if (value == "Merchant") {
      setMerchantActive(true);
    } else {
      setMerchantActive(false);
    }
    if (value == "Transaction") {
      setTransactionActive(true);
    } else {
      setTransactionActive(false);
    }
    if (value == "Social") {
      setSocialActive(true);
    } else {
      setSocialActive(false);
    }
    console.log(value);
    evt.preventDefault();
  };
  useEffect(() => {
    if (props.merchantId != "") {
      var merchantURL =
        "http://3.108.30.61:9000/social_analysis_api/mid?merchant_id=" +
        props.merchantId;
      var merchantFraudURL =
        "http://3.108.30.61:8000/merchant_fraud_score/MRI?merchant_id=" +
        props.merchantId;
      var merchantRecordURL =
        "http://3.108.30.61:8000/merchant_fraud_score/MRA?merchant_id=" +
        props.merchantId;
      var merchantTransactionalURL =
        "http://3.108.30.61:8000/merchant_fraud_score/MTA?merchant_id=" +
        props.merchantId;
    } else {
      var merchantURL =
        "http://3.108.30.61:9000/social_analysis_api/murl/?merchant_url=" +
        props.merchantURL;
    }
    axios.get(merchantURL).then((res) => {
      const response = res.data;
      const data = response.data;
      const domain_age_ip_details = data.Domain_age_IP_details;
      const domain_riskness_status_details =
        data.Domain_Category_Riskiness_Status_details;
      const website_webpage_analsis_details = data.Website_webpage_analysis;
      const website_response = data["website response"];
      const google_review_detail = data["google reviews response"];
      const twitter_response = data["twitter response"];
      const domain_rank_info =
        data["Domain_Ranking_Details"]["Domain rank info"]["rank"];
      console.log(domain_rank_info);
      setDomainRank(domain_rank_info["global"]);
      if (google_review_detail["google reviews"] != null) {
        setGoogleReviewRating(google_review_detail["google reviews"][0]);
        setGoogleReviewCount(google_review_detail["google reviews"][1]);
      }
      setTwitterAccountExist(twitter_response["Twitter account"]);
      setTwitterFollowerCount(twitter_response["Followers"]);
      setTweetCount(twitter_response["tweets"]);
      const social_media_link = website_response["Social platforms"];
      const website_url = data.website_url;
      setWebsiteURL(website_url);
      setSocialMediaLink(social_media_link);
      setLoadingFlag(true);
      if (website_response["is website exists"] == "1") {
        setWebsiteExist("Yes");
      } else {
        setWebsiteExist("No");
      }
      const google_reviews_details = data["google reviews response"];
      setTotalWebPage(
        website_webpage_analsis_details["Total_webpage in a website"]
      );
      setTermConditionPrivacy(
        website_webpage_analsis_details.TermCondition_PrivacyPolicy_RefundPolicy_pagelink
      );
      setAge(domain_age_ip_details.Domain_age);
      setCreationDate(domain_age_ip_details.Domain_creation_date);
      setDomainCategory(domain_riskness_status_details.Domain_category);
      setRiskinessStatus(domain_riskness_status_details.Riskiness_status);
      setNumSocialMedia(website_response["Number of social media"]);
      if (domain_age_ip_details.Blacklist_status == "1") {
        setBlackListStatus("Yes");
      } else {
        setBlackListStatus("No");
      }
    });
    axios.get(merchantFraudURL).then((res) => {
      const response = res.data;
      const data = response.data;
      setFraudData(data);
    });
    axios.get(merchantRecordURL).then((res) => {
      const response = res.data;
      const data = response.data;
      const checked_merchant_id = data.checked_merchant_id;
      const Email_id = data.Email_id;
      const mobile_whatsapp_status = data.mobile_whatsapp_status;
      const business_name = data.Business_name;
      const mobile_no = data.Mobile_no;
      const comnpanyResponse = data["Company_response"];
      const CIN_Number = comnpanyResponse.CIN_Number;
      const Company_Profile = comnpanyResponse["Company_Profile"];
      const MMatchedId = data.Matched_Merchant_ID;
      const FraudulentMerchant = data["Count of matched fraudulent merchant"];
      const TerminatedMID = data["Id of matched fraudulent merchant"];
      const Matched_Merchant_ID = data.Matched_Merchant_ID;
      const Matched_Mobile_number = data.Matched_Mobile_number;
      const Matched_Website_url = data.Matched_Website_url;
      const Matched_Email = data.Matched_Email;
      const Matched_PAN = data.Matched_PAN;
      const Matched_GSTIN = data.Matched_GSTIN;
      const merchant_email_count = data.merchant_email_count;
      const merchant_pan_no_count = data.merchant_pan_no_count;
      const merchant_bank_name_count = data.merchant_bank_name_count;
      const image_count = data.image_count;
      const merchant_websiteurl_count = data.merchant_websiteurl_count;
      const merchant_gstin_details = data.gstin_details;
      const merchant_email_details = data.Email_details;
      const pan_type = data.pan_type;
      const court_cases = data["Court Cases"];
      console.log(natureOfBusinessActivities);
      setCountMatchMerchantFraud(data["Count of matched fraudulent merchant"]);
      setMerchantEmailCount(data["merchant_email_count"]);
      setMerchantPanNoCount(data["merchant_pan_no_count"]);
      setMerchantImageCount(data["image_count"]);
      setMerchantWebsiteUrlCount(data["merchant_websiteurl_count"]);
      setMerchantBankNameCount(data["merchant_bank_name_count"]);
      setBankFraudCount(data["bank_fraud_count"]);
      setStateFraudCount(data["state_fraud_count"]);
      setPrimaryMobileNumber(data["Mobile_no."]);
      setMerchantBankName(data["bank"]);
      setMerchantState(data["state"]);
      setMatchedMobileNumber(data["Matched_Mobile_number"]);
      setMatchedPan(data["Matched_PAN"]);
      setMatchedWebsiteUrl(data["Matched_Website_url"]);
      setMatchedEmail(data["Matched_Email"]);
      setMatchedGST(data["Matched_GSTIN"]);
      setPanType(pan_type);
      setMatchedMerchantID(data["Matched_Merchant_ID"]);
      if (data["IP_location"] != null) {
        setIpRegion(data["IP_location"]["Ip_region"]);
      }
      if (comnpanyResponse["Director_data"] != null) {
        setDirectorDetail(comnpanyResponse["Director_data"]);
      }
      if (court_cases != null) {
        setSupremeCourtCases(court_cases["Supreme Court Cases"]);
        setHighCourtCases(court_cases["High Court Cases"]);
      }
      if (merchant_gstin_details != null) {
        setGSTNo(merchant_gstin_details["gstin_no"]);
        setConstitutionOfBusiness(
          merchant_gstin_details["constitutionOfBusiness"]
        );
        setLegalNameOfBusiness(merchant_gstin_details["legalNameOfBusiness"]);
        setCentreJurisdiction(merchant_gstin_details["centreJurisdiction"]);
        setStateJurisdiction(merchant_gstin_details["stateJurisdiction"]);
        setRegistrationDate(merchant_gstin_details["registrationDate"]);
        setTaxPayerType(merchant_gstin_details["taxPayerType"]);
        setGstinStatus(merchant_gstin_details["gstinStatus"]);
        setPrincipalPlaceAddress(
          merchant_gstin_details["principalPlaceAddress"]
        );
        setNatureOfBusinessActivities(
          merchant_gstin_details["natureOfBusinessActivities"]
        );
      }
      if (merchant_email_details != null) {
        setEmailAddressId(merchant_email_details["emailId"]);
        setValidEmail(merchant_email_details["validEmail"]);
        setEmailStatus(merchant_email_details["status"]);
        setFreeEmail(merchant_email_details["freeEmail"]);
        setDomainEmail(merchant_email_details["domain"]);
        setDomainAgeDays(merchant_email_details["domainAgeDays"]);
        setSmtpProvider(merchant_email_details["smtpProvider"]);
      }
      setCheckedMerchantID(checked_merchant_id);
      setEmailID(Email_id);
      setWhatsappStatus(mobile_whatsapp_status);
      setBusinessName(business_name);
      setMobileNumber(mobile_no);
      setCiNumber(CIN_Number);
      setCompanyName(Company_Profile.COMPANY_NAME);
      setDor(Company_Profile.DATE_OF_REGISTRATION);
      setCat(Company_Profile.CATEGORY);
      setClassDevision(Company_Profile.CLASS);
      setSubCat(Company_Profile.SUBCATEGORY);
      setRoa(Company_Profile.REGISTERED_OFFICE_ADDRESS);
      setOfficeEmail(Company_Profile.EMAIL);
      if (MMatchedId != null) {
        setMRecordsExists("Yes");
      } else {
        setMRecordsExists("No");
      }
      setMatchedId(MMatchedId);
      setFraudulentMerchant(FraudulentMerchant);
      setTerminatedMID(TerminatedMID);
      setRelMobileNumber(Matched_Merchant_ID);
      setRelEmail(Matched_Mobile_number);
      setRelWebsite(Matched_Website_url);
      setRelPanCard(Matched_Email);
      setRelGST(Matched_GSTIN);
      setCountEmailChange(merchant_email_count);
      setCountPanChange(merchant_pan_no_count);
      setCountBankNameChange(merchant_bank_name_count);
      setCountImageChange(image_count);
      setCountWebsiteChange(merchant_websiteurl_count);
    });
    axios.get(merchantTransactionalURL).then((res) => {
      const response = res.data;
      const data = response.data;
      const easebuzz_id = data.easebuzz_id;
      const peb_merchant_id = data.peb_merchant_id;
      const signup_date = data.signup_date;
      const merchant_name = data.merchant_name;
      const submerchant_id = data.submerchant_id;
      const kyc_upload_date = data.kyc_upload_date;
      const bank_details_upload_date = data.bank_details_upload_date;
      const business_info_upload_date = data.business_info_upload_date;
      const first_txn_date = data.first_txn_date;
      const last_txn_date = data.last_txn_date;
      const txn_count = data.txn_count;
      const gmv = data.gmv;
      const recency_metric = data.recency_metric;
      const max_txn_amt = data.max_txn_amt;
      const min_txn_amt = data.min_txn_amt;
      const txn_amt_mean = data.txn_amt_mean;
      const unique_cust_count = data.unique_cust_count;
      const max_cust_txn_count = data.max_cust_txn_count;
      const sigma = data.sigma;
      const recency = data.recency;
      const frequency = data.frequency;
      const monetary = data.monetary;
      const rfm_value = data.rfm_value;
      const rfm_avg = data.rfm_avg;
      const rfm_category = data.rfm_category;
      const lifetime_median_amt = data.lifetime_median_amt;
      const lifetime_mode_amt = data.lifetime_mode_amt;
      const lifetime_refund_amount = data.lifetime_refund_amount;
      const lifetime_refund_count = data.lifetime_refund_count;
      const lifetime_chargeback_amount = data.lifetime_chargeback_amount;
      const lifetime_chargeback_count = data.lifetime_chargeback_count;
      const is_sub_aggregator = data.is_sub_aggregator;
      const last_txn_since_days = data.last_txn_since_days;
      const business_domain = data.business_domain;
      const is_easycollect_merchant = data.is_easycollect_merchant;
      const is_smartbilling_merchant = data.is_smartbilling_merchant;
      const is_edu_merchant = data.is_edu_merchant;
      const is_bridgebuzz_merchant = data.is_bridgebuzz_merchant;
      const is_wire_merchant = data.is_wire_merchant;
      const login_date = data.login_date;
      const login_ips = data.login_ips;
      const signup_ips = data.signup_ips;
      setEzId(easebuzz_id);
      setEzMName(merchant_name);
      setFirstTransactionDate(first_txn_date);
      setTransactionAmountMean(txn_amt_mean);
      setUniqueCustomerCount(unique_cust_count);
      setCustomerTransactionCount(max_cust_txn_count);
      setSigma(sigma);
      setRecency(recency);
      setFrequency(frequency);
      setMonetary(monetary);
      setRfmValue(rfm_value);
      setRfmAverage(rfm_avg);
      setRfmCategory(rfm_category);
      setLifetimeMedianAmount(lifetime_median_amt);
      setLifetimeRefundAmount(lifetime_refund_amount);
      setLifetimeRefundCount(lifetime_refund_count);
      setLifetimeChargebackAmount(lifetime_chargeback_amount);
      setLifetimeChargebackCount(lifetime_chargeback_count);
      setAggregator(is_sub_aggregator);
      setSignupDate(signup_date);
      setLoginDate(login_date);
      setUploadDate(kyc_upload_date);
      setDetailsUploadDate(bank_details_upload_date);
      setInfoUploadDate(business_info_upload_date);
      setBusinessDomain(business_domain);
      setEaseCollectMerchant(is_easycollect_merchant);
      setSmartBillingMerchant(is_smartbilling_merchant);
      setLoginIPs(login_ips);
      setEduMerchant(is_edu_merchant);
      setBridgeBuzzMerchant(is_bridgebuzz_merchant);
      setWireMerchant(is_wire_merchant);
    });
  }, []);
  if (loadingFlag) {
    return (
      <section className="tab_section_page_layout">
        <ul class="tab_nav_ul">
          <li>
            <a
              href="javascript:void(0)"
              title="Social Analysis"
              className={isSocialActive ? "active" : null}
              onClick={(e) => showTabSection(e, "Social")}
            >
              Social Analysis
            </a>
          </li>
          <li>
            <a
              href="javascript:void(0)"
              title="Merchant Fraud Analysis"
              className={isMerchantActive ? "active" : null}
              onClick={(e) => showTabSection(e, "Merchant")}
            >
              Merchant Fraud Analysis
            </a>
          </li>
          <li>
            <a
              href="javascript:void(0)"
              title="Transactional Analysis"
              className={isTransactionActive ? "active" : null}
              onClick={(e) => showTabSection(e, "Transaction")}
            >
              Transactional Analysis
            </a>
          </li>
        </ul>
        <div
          className="row  page_loading_content_section"
          style={{ display: isSocialActive ? "flex" : "none" }}
        >
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">Website Information</div>
              <div className="loading_content_body">
                <div className="loading_content_label">
                  <strong>Website Exist :</strong>
                  <span className="loading_content_value"> {websiteExist}</span>
                </div>
                <div className="loading_content_label">
                  <strong>Website URL :</strong>
                  <span className="loading_content_value"> {websiteURL}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">Social Media Info</div>
              <div className="loading_content_body">
                <div className="loading_content_label">
                  <strong>Number of social media : </strong>
                  <span className="loading_content_value">
                    {" "}
                    {numSocialMedia}
                  </span>
                </div>
                <div className="loading_content_label">
                  <strong>Social platforms :</strong>
                  <span className="loading_content_value"></span>
                </div>
                <ul className="social_media_nav_ul">
                  {socialMediaLink.map((links) => (
                    <li>
                      <li>
                        <a href={links}>{links}</a>
                      </li>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">
                Google and Twitter Stats
              </div>
              <div className="loading_content_body">
                <div className="loading_content_label">
                  <strong>Google reviews :</strong>
                  <div className="loading_content_value sub_content_value">
                    Review Rating : {googleReviewRating}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Review Count : {googleReviewCount}
                  </div>
                </div>
                <div className="loading_content_label">
                  <strong>Twitter Info :</strong>
                  <div className="loading_content_value sub_content_value">
                    {" "}
                    {twitterAccountExist}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Follwers : {twitterFollowerCount}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Tweets : {twitterTweetCount}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">Business related info</div>
              <div className="loading_content_body">
                <div className="loading_content_label">
                  <strong>Domain Age IP Details :</strong>
                  <div className="loading_content_value sub_content_value">
                    Blacklist status : {blackListStatus}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Date : {creationDate}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Age : {age}
                  </div>
                </div>
                <div className="loading_content_label">
                  <strong>Domain Category Riskiness Status details :</strong>
                  <div className="loading_content_value sub_content_value">
                    Riskiness Status :{riskinessStatus}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Category :{domainCategory}
                  </div>
                </div>
                <div className="loading_content_label">
                  <strong>Website Webpage Analysis :</strong>
                  <div className="loading_content_value sub_content_value">
                    Domain rank info : {domainRank}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Total Webpage in a website : {totalWebPage}
                  </div>
                  <div className="loading_content_value sub_content_value">
                    Term Condition PrivacyPolicy Refund Policy Pagelink :<br />
                    <br />
                    {termConditionPrivacy.map((links) => (
                      <li>
                        <a href={links}>{links}</a>
                      </li>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="row  page_loading_content_section"
          style={{ display: isMerchantActive ? "flex" : "none" }}
        >
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">Merchant Details</div>
              <div className="loading_content_body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Merchant Id
                      <div className="loading_content_value">
                        {checkedMerchantID}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Mobile No
                      <div className="loading_content_value">
                        {" "}
                        {primaryMobileNumber}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Whatsapp Status
                      <div className="loading_content_value">
                        {whatsappStatus}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant Email id
                      <div className="loading_content_value">{emailID}</div>
                    </div>
                    <div className="loading_content_label">
                      Email Domain
                      <div className="loading_content_value">{domainEmail}</div>
                    </div>
                    <div className="loading_content_label">
                      Email Id Domain Age
                      <div className="loading_content_value">
                        {domainAgeDays}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Email Id SMTP Provider
                      <div className="loading_content_value">
                        {smtpProvider}
                      </div>
                    </div>
                    {/* 
                  <div className="loading_content_label">
                     Merchant Email Id
                     <div className="loading_content_value">{emailAddressId}</div>
                  </div>
                  */}
                    <div className="loading_content_label">
                      Is Emailid Valid
                      <div className="loading_content_value">{validEmail}</div>
                    </div>
                    <div className="loading_content_label">
                      Email Id Status
                      <div className="loading_content_value">{emailStatus}</div>
                    </div>
                    <div className="loading_content_label">
                      Is if Free Email
                      <div className="loading_content_value">{freeEmail}</div>
                    </div>
                    <div className="loading_content_label">
                      Merchant Bank Name
                      <div className="loading_content_value">
                        {merchantBankName}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant Bank Wise Fraud
                      <div className="loading_content_value">
                        {bankFraudCount}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant State Name
                      <div className="loading_content_value">
                        {merchantState}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant State Wise Fraud
                      <div className="loading_content_value">
                        {stateFraudCount}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Business name :
                      <div className="loading_content_value">{businesName}</div>
                    </div>
                    <div className="loading_content_label">
                      Company CIN number :
                      <div className="loading_content_value">{ciNumber}</div>
                    </div>
                    <div className="loading_content_label">
                      Company profile :
                      <div className="loading_content_value">
                        <small>
                          Company Name: {companyName}
                          <br />
                          Registration No: {dor}
                          <br />
                          Category: {cat}
                          <br />
                          Class: {classDevision}
                          <br />
                          Subcategory: {subCat}
                          <br />
                          Office Address: {roa}
                          <br />
                          E-Mail: {officeEmail}
                        </small>
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Company Director
                      <div className="loading_content_value">
                        <ul className="social_media_nav_ul">
                          {directorDetail.map((director, index) => (
                            <li>
                              <li>{director[0]}</li>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant Pan type
                      <div className="loading_content_value">{panType}</div>
                    </div>
                    <div className="loading_content_label">
                      Merchant activities IP&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          IP location based on merchant activity
                        </p>
                      </span>
                      <div className="loading_content_value">{ipRegion}</div>
                    </div>
                    <div className="loading_content_label">
                      Supreme Court Cases&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Number of court case on merchant in Supreme court
                        </p>
                      </span>
                      <div className="loading_content_value">
                        {supremeCourtCases}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      High Court Cases&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Number of court case on merchant in High court
                        </p>
                      </span>
                      <div className="loading_content_value">
                        {highCourtCases}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-8">
            <div className="row">
              <div className="col-md-6">
                <div className="loading_content_box">
                  <div className="loading_content_head">Fraud score</div>
                  <div className="loading_content_body">
                    <div className="loading_content_label">
                      <span className="loading_content_value">{fraudData}</span>
                    </div>
                  </div>
                </div>
                <div className="loading_content_box">
                  <div className="loading_content_head">Merchant Records</div>
                  <div className="loading_content_body">
                    <div className="loading_content_label">
                      Merchant record exist in easebuzz records?&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Does Merchant records exists in Past Easebuzz Eecords?
                        </p>
                      </span>
                      <div className="loading_content_value">
                        {mRecordsExists}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Matched merchant ID&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          With which merchant id, this merchant id records
                          matches.
                        </p>
                      </span>
                      <div className="loading_content_value">
                        {matchedMerchantID}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Past Fraudulent matched merchant&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Is any matched merchant were fradulent in the past?
                        </p>
                      </span>
                      <div className="loading_content_value">-</div>
                    </div>
                    <div className="loading_content_label">
                      Count of matched fraudulent merchant&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          How many matched merchant were fraudulent in the past?
                        </p>
                      </span>
                      <div className="loading_content_value">
                        {countMatchMerchantFraud}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="loading_content_box">
                  <div className="loading_content_head">
                    Registration details
                  </div>
                  <div className="loading_content_body">
                    <div className="loading_content_label">
                      Related mobile numbers&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Mobile Numbers which is related to current merchant.
                        </p>
                      </span>
                      <div className="loading_content_value">
                        {matchedMobileNumber.map((matchmobile) => (
                          <li>
                            <li>{matchmobile}</li>
                          </li>
                        ))}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Related Email&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Emails which is related to current merchant.
                        </p>
                      </span>
                      <div className="loading_content_value">
                        <ul className="social_media_nav_ul">
                          {matchedEmail.map((matchemail) => (
                            <li>
                              <li>{matchemail}</li>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Related websites&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Websites which is related to current merchant.
                        </p>
                      </span>
                      <div className="loading_content_value">
                        <ul className="social_media_nav_ul">
                          {matchedWebsiteUrl.map((websiteUrl) => (
                            <li>{websiteUrl}</li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Related Pan cards&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Pan Cards which is related to current merchant.
                        </p>
                      </span>
                      <div className="loading_content_value">
                        <ul className="social_media_nav_ul">
                          {matchedPan.map((matchpan) => (
                            <li>{matchpan}</li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Related GST number&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          GST number which is related to current merchant.
                        </p>
                      </span>
                      <div className="loading_content_value">
                        <ul className="social_media_nav_ul">
                          {matchedGST.map((matchgst) => (
                            <li>{matchgst}</li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Number of times merchant changed their details&nbsp;
                      <span className="Tooltips_icons">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="16"
                          height="16"
                          fill="currentColor"
                          className="bi bi-info-circle"
                          viewBox="0 0 16 16"
                        >
                          <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                          <path d="m8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
                        </svg>
                        <p className="Tooltips_value">
                          Number of times merchant has changed their data(1
                          means he didn't change after filling up the form and 0
                          means he didn't provide that info)
                        </p>
                      </span>
                      <div className="loading_content_value"></div>
                    </div>
                    <div className="loading_content_label">
                      Email: &nbsp;
                      <span className="loading_content_value">
                        {merchantEmailCount}
                      </span>
                    </div>
                    <div className="loading_content_label">
                      Pan Number: &nbsp;{" "}
                      <span className="loading_content_value">
                        {merchantPanNoCount}
                      </span>
                    </div>
                    <div className="loading_content_label">
                      Bank Name &nbsp;{" "}
                      <span className="loading_content_value">
                        {merchantBankNameCount}
                      </span>
                    </div>
                    <div className="loading_content_label">
                      Image &nbsp;{" "}
                      <span className="loading_content_value">
                        {merchantImageCount}
                      </span>
                    </div>
                    <div className="loading_content_label">
                      Website URL &nbsp;{" "}
                      <span className="loading_content_value">
                        {merchantWebsiteUrlCount}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="loading_content_box">
                  <div className="loading_content_head">GST details</div>
                  <div className="loading_content_body">
                    <div className="loading_content_label">
                      Gstin number
                      <div className="loading_content_value">{gstNo}</div>
                    </div>
                    <div className="loading_content_label">
                      Merchant constitution Of Business
                      <div className="loading_content_value">
                        {constitutionOfBusiness}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant legal Name Of Business
                      <div className="loading_content_value">
                        {legalNameOfBusiness}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant business centre Jurisdiction
                      <div className="loading_content_value">
                        {centreJurisdiction}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant business state Jurisdiction
                      <div className="loading_content_value">
                        {stateJurisdiction}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant business registration Date
                      <div className="loading_content_value">
                        {registrationDate}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant Tax payer type
                      <div className="loading_content_value">
                        {taxPayerType}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant Gstin Status
                      <div className="loading_content_value">{gstinStatus}</div>
                    </div>
                    <div className="loading_content_label">
                      Merchant nature of business activities
                      <div className="loading_content_value">
                        {natureOfBusinessActivities}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Merchant business principal Place Address
                      <div className="loading_content_value">
                        {principalPlaceAddress}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="row  page_loading_content_section"
          style={{ display: isTransactionActive ? "flex" : "none" }}
        >
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">Merchant Details</div>
              <div className="loading_content_body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Merchant id
                      <div className="loading_content_value">{ezId}</div>
                    </div>
                    <div className="loading_content_label">
                      Merchant name
                      <div className="loading_content_value">{ezMName}</div>
                    </div>
                    <div className="loading_content_label">
                      First transaction date
                      <div className="loading_content_value">
                        {" "}
                        {firstTransactionDate}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Transaction amount mean
                      <div className="loading_content_value">
                        {" "}
                        {transactionAmountMean}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Unique Customer Count
                      <div className="loading_content_value">
                        {uniqueCustomerCount}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Maximum Customer Transaction Count
                      <div className="loading_content_value">
                        {" "}
                        {customerTransactionCount}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Sigma
                      <div className="loading_content_value"> {Sigma}</div>
                    </div>
                    <div className="loading_content_label">
                      Recency
                      <div className="loading_content_value"> {recency}</div>
                    </div>
                    <div className="loading_content_label">
                      Frequency
                      <div className="loading_content_value"> {frequency}</div>
                    </div>
                    <div className="loading_content_label">
                      Monetary
                      <div className="loading_content_value"> {monetary}</div>
                    </div>
                    <div className="loading_content_label">
                      RFM Value
                      <div className="loading_content_value"> {rfmValue}</div>
                    </div>
                    <div className="loading_content_label">
                      RFM Average
                      <div className="loading_content_value"> {rfmAverage}</div>
                    </div>
                    <div className="loading_content_label">
                      RFM Category
                      <div className="loading_content_value">
                        {" "}
                        {rfmCategory}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">Lifetime</div>
              <div className="loading_content_body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Lifetime Median Amount
                      <div className="loading_content_value">
                        {lifetimeMedianAmount}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Lifetime Refund Amount
                      <div className="loading_content_value">
                        {lifetimeRefundAmount}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Lifetime Refund Count
                      <div className="loading_content_value">
                        {lifetimeRefundCount}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Lifetime Chargeback Amount
                      <div className="loading_content_value">
                        {lifetimeChargebackAmount}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Lifetime Chargeback Count
                      <div className="loading_content_value">
                        {lifetimeChargebackCount}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Is Sub Aggregator
                      <div className="loading_content_value">{aggregator}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="loading_content_box">
              <div className="loading_content_head">
                Merchant KYC Data Analysis
              </div>
              <div className="loading_content_body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Signup Date
                      <div className="loading_content_value">{signupDate}</div>
                    </div>
                    <div className="loading_content_label">
                      Login Date
                      <div className="loading_content_value">{loginDate}</div>
                    </div>
                    <div className="loading_content_label">
                      KYC Upload Date
                      <div className="loading_content_value"> {uploadDate}</div>
                    </div>
                    <div className="loading_content_label">
                      Bank Details Upload Date
                      <div className="loading_content_value">
                        {" "}
                        {detailsUploadDate}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Business Info Upload Date
                      <div className="loading_content_value">
                        {infoUploadDate}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Business Domain
                      <div className="loading_content_value">
                        {" "}
                        {businessDomain}
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="loading_content_label">
                      Is EaseCollect Merchant
                      <div className="loading_content_value">
                        {easeCollectMerchant}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Is SmartBilling Merchant
                      <div className="loading_content_value">
                        {" "}
                        {smartBillingMerchant}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Login IPs
                      <div className="loading_content_value"> {loginIPs}</div>
                    </div>
                    <div className="loading_content_label">
                      Is Edu Merchant
                      <div className="loading_content_value">
                        {" "}
                        {eduMerchant}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Is BridgeBuzz Merchant
                      <div className="loading_content_value">
                        {" "}
                        {bridgeBuzzMerchant}
                      </div>
                    </div>
                    <div className="loading_content_label">
                      Is Wire Merchant
                      <div className="loading_content_value">
                        {" "}
                        {wireMerchant}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  } else {
    return (
      <section>
        <div id="loading">
          <div class="loading">
            <img src={window.location.origin + "/loading.gif"} />
          </div>
        </div>
      </section>
    );
  }
}
export default TabDetail;
