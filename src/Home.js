import React, { Component }  from 'react';
import Header from './Header';
import './css/style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from './Footer';
import TabDetail from './TabDetail';
import { useState } from "react"
function Home(){
    const [merchantId, setMerchantId] = useState("");
    const [tabflag, setTabDetailFlag] = useState(false);
    const [merchantURL, setMerchantURL] = useState("");
   
    const merChantIdFormSubmit = (evt) => {
       
        setTabDetailFlag(false);
        console.log(tabflag);
        //alert(`Submitting MerchantId ${merchantId}`);
        if(merchantId.trim()==""){
            alert("Please Enter Merchant Id");
            setMerchantId("");
        }else{
            setMerchantId(merchantId.trim());
            setTabDetailFlag(true);
        }
        evt.preventDefault();
    }
    function handleMerchantIdValues(value) {
        setTabDetailFlag(false);
        setMerchantId(value);
        setMerchantURL("");
    }
    function handleMerchantURLValues(value) {
        setTabDetailFlag(false);
        setMerchantURL(value);
        setMerchantId("");
    }
    const merChantURLFormSubmit = (evt) => {
       
        //alert(`Submitting MerchantId ${merchantId}`);
        setTabDetailFlag(false);
        if(merchantURL.trim()==""){
            alert("Please Enter Merchant URL");
            setMerchantURL("");
        }else{
            setMerchantURL(merchantURL.trim());
            setTabDetailFlag(true);
        }
        evt.preventDefault();
    }
    if (tabflag) {
        var tabDetail = <TabDetail merchantId={merchantId} merchantURL={merchantURL}/>;
    } else {
        var tabDetail = null;
    }
    return (<div>
        <Header></Header>
        <div class="container">
            <section className="merchant_form_section">
                <div className="row">
                    <div className="col-md-5">
                    <form onSubmit={merChantIdFormSubmit}>
                        <div className="merchant_id_form row">
                            <div className="col-9">
                                <label for="merchant_id" className="col-form-label">Merchant ID</label>
                                <input type="text" className="form-control" id="merchant_id"
                                value={merchantId} onChange={e => handleMerchantIdValues(e.target.value)}
                                />
                            </div>
                            <div className="col-3">
                                <label className="col-form-label" style={{visibility: 'hidden'}}>blank</label>
                                <button type="submit" className="btn btn-primary submit_button">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-md-2" style={{textAlign: 'center'}}>
                    <div className="or_tr_row">
                        <label for="merchant_id" className="col-form-'label">OR</label>
                    </div>
                </div>
                <div className="col-md-5">
                    <form onSubmit={merChantURLFormSubmit}>
                        <div class="merchant_id_form row">
                            <div class="col-9">
                                <label for="merchant_Url" className="col-form-label">Merchant Url</label>
                                <input type="text" className="form-control" id="merchant_Url"  value={merchantURL} onChange={e => handleMerchantURLValues(e.target.value)} />
                            </div>
                            <div className="col-3">
                                <label className="col-form-label" style={{visibility: 'hidden'}}>blank</label>
                                <button type="submit" className="btn btn-primary submit_button">Submit</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
                </div>
            </section>
            {tabDetail }
        </div>
        <Footer></Footer>
    </div>)
}
export default Home;