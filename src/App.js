import React, { Component }  from 'react';
import Home from './Home.js';
import Login from './Login.js';
import { useState, useEffect  } from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
function getLoginSessionFlag() {
  
    const loginFlag = sessionStorage.getItem('loginFlag');
   // const userToken = JSON.parse(tokenString);
    return loginFlag

}
function App() {
  //const loginFlag = useState(false);
  const loginFlag = getLoginSessionFlag();
  if(!loginFlag){
      return <Login></Login>;
  }else{
      return <Home></Home>;
  }
  return (
    <div className="App">
       <BrowserRouter>
        <Switch>
          <Route path="/home">
            <Home />
          </Route>
         
        </Switch>
      </BrowserRouter>
      if(!loginFlag){
          <Login></Login>
      }else{
        <Home/>
      }
      
    </div>
  );
}

export default App;
